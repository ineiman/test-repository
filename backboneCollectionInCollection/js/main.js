
window.template = function(id) {
	return _.template($('#'+id).html());
}

MyApp = new Backbone.Marionette.Application();
MyApp.addRegions({
	mainRegion: '#content',
	subTable : '.subTable'
});



MyApp.ContentApp = function() {
	var ContentApp = {};

	return ContentApp;
}
MyApp.ContentApp.SubRow = Backbone.Model.extend({
	defaults:{
		title:'Item#1',
		firstInfo:'Column Info',
		secondInfo: 'Column Info'
	}
});
MyApp.ContentApp.SubRows = Backbone.Collection.extend({
	model : MyApp.ContentApp.SubRow
});


MyApp.ContentApp.Row = Backbone.Model.extend({
	defaults:{
		title	:'Req8436',
		day		:'mm/dd/yyyyk',
		status	:'Med closet 123',
		price	:'$480.05'
	},
	subRows: new MyApp.ContentApp.SubRows()
});

MyApp.ContentApp.SubRowView = Backbone.Marionette.ItemView.extend({
	template: template('subRowTemplate'),
	tagName:'tr'
})

MyApp.ContentApp.SubRowsView = Backbone.Marionette.CompositeView.extend({
	template: template('subTableTemplate'),
	tagName: 'td',
	
	itemView: MyApp.ContentApp.SubRowView ,

	appendHtml:function (collectionView, itemView) {
		collectionView.$('#subTableContent').append(itemView.el);
		this.$el.attr('colspan','5');
	}
	
})
MyApp.ContentApp.Rows = Backbone.Collection.extend({
	model: MyApp.ContentApp.Row
});
MyApp.ContentApp.RowView = Backbone.Marionette.ItemView.extend({
	template	: template('rowTemplate'),
	tagName 	: 'tr',
	
	events: {
				'click':'showSubTable'
	},
	showSubTable:function(e) {		

		if (e.target) {
			var subConteiner = e.target.parentNode;
			var hideAllFlag = $(subConteiner).next(".subTable").size();
			var tableRows = $(subConteiner).siblings(".subTable");
			var activeRow = $(subConteiner).siblings(".activeRow");
			if (!hideAllFlag) {

				if (tableRows.size()) {
					tableRows.remove();
					activeRow.attr('class','');
					this.$el.attr('class','');
				}
				var subRows = this.model.get('subRows');
				if(!subRows){
					return this;
				}
				if(parseInt(subRows.length) === 0) {
					return this;
				}
				var subRowsView = new MyApp.ContentApp.SubRowsView({collection: subRows});	

				this.$el.after('<tr class="subTable"></tr>');
				this.$el.attr('class','activeRow');
				MyApp.addRegions({
					subTable : '.subTable'
				});
				
				MyApp.subTable.show(subRowsView);

			}else {
				tableRows.remove();
				activeRow.attr('class','');
				this.$el.attr('class','');
			}				
		};
		
		return this;
	}
});
MyApp.ContentApp.RowsView = Backbone.Marionette.CompositeView.extend({
	template	: template('tableTemplate'),
	tagName		: 'table',
	className	: 'table table-hover',
	itemView 	: MyApp.ContentApp.RowView,

	appendHtml 	: function(collectionView, itemView) {
		collectionView.$('#mainTableContent').append(itemView.el);
	}
});
MyApp.on('initialize:after', function () {
	var subRows = new MyApp.ContentApp.SubRows([
		{
		title:'Item#1',
		firstInfo:'Column Info',
		secondInfo: 'Column Info'
	},
	{
		title:'Item#1',
		firstInfo:'Column Info',
		secondInfo: 'Column Info'
	},
	{
		title:'Item#1',
		firstInfo:'Column Info',
		secondInfo: 'Column Info'
	}
		]);
	var row = new MyApp.ContentApp.Row({

	});
	var rows = new MyApp.ContentApp.Rows([
		{
			title	:'Req8436',
			day		:'mm/dd/yyyyk',
			status	:'Med closet 123',
			price	:'$480.05',
			
		},
		{
			title	:'Req8436',
			day		:'mm/dd/yyyyk',
			status	:'Med closet 123',
			price	:'$480.05',
			subRows :subRows
		},
		{
			title	:'Req8436',
			day		:'mm/dd/yyyyk',
			status	:'Med closet 123',
			price	:'$480.05',
			subRows :subRows
		},
		{
			title	:'Req8436',
			day		:'mm/dd/yyyyk',
			status	:'Med closet 123',
			price	:'$480.05',
			subRows :subRows
		}

	]);

	var rowsView = new MyApp.ContentApp.RowsView({
		collection:rows
	});

	MyApp.mainRegion.show(rowsView);
});


$(function () {
	MyApp.start();
})