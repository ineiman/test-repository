$(function () {
	window.App ={
		Models: {},
		Views: {},
		Collections: {},
		Router:{}
	};

	window.template = function(id){
		return _.template($('#'+id).html() );
	};

	var vent = _.extend({}, Backbone.Events);


	App.Router = Backbone.Router.extend({
		routes:{
			'' 						: 'index',
			'task/:id'		    	: 'showTask',
			'*other'				: 'default'
		},
		index:function () {
			
		},
		showTask:function (id) {
			vent.trigger('taskId:show', id);
		},
		default:function (other) {
			alert('Are you shure that you want go to '+ other);
		},
	});

	

	App.Models.Task = Backbone.Model.extend({
		validate: function (attrs) {
			if(!$.trim(attrs.title)){
				return 'Invalid title';
			}
		}
	});
	App.Views.Task = Backbone.View.extend({
		initialize:function () {
			this.model.on('change', this.render, this);	
			this.model.on('destroy', this.remove,this);
			
		},
		tagName: 'li',
		template: template('taskTemplate'),

		render: function () {
			var template = this.template(this.model.toJSON());
			this.$el.html(template);
			return this;
		},
		events:{
			'click .edit':'editTask',
			'click .delete':'destroy'
		},
		editTask:function () {
			var newTaskTitle = prompt('How did you want rename the task?',this.model.get('title'));
			this.model.set('title',newTaskTitle,{validate:true});
		},
		destroy:function () {
			this.model.destroy();
		},
		remove:function() {
			this.$el.remove();
		}
	});

	
	App.Collections.Task = Backbone.Collection.extend({
		model: App.Models.Task
	});

	App.Views.Tasks = Backbone.View.extend({
		tagName: 'ul',
		
		initialize:function () {
			this.collection.on('add',this.addOne,this);
			vent.on('taskId:show', this.show, this);
		},
		render: function () {
			
			this.collection.each(this.addOne , this);

			return this;
		},
		addOne: function(task){
			var taskView = new App.Views.Task({model: task});
			this.$el.append(taskView.render().el)
		},
		show : function (id) {
			var taskId = this.collection.get(id);
			var taskView = new App.Views.Task({model:taskId});
			$('body').append(taskView.render().el);
			console.log(taskView);			
		}
	});
	var tasks = new App.Collections.Task([
		{ 
			id : 1,
			title:'Go to work',
			priority: 5
		},
		{
			id : 2,
			title: 'Get letters',
			priority: 3
		},
		{	
			id : 3,
			title: 'Go to the shop',
			priority: 4
		}
			]);
	var tasksView = new App.Views.Tasks({collection: tasks});
	new App.Views.Tasks({collection: tasks});
	new App.Router();
	Backbone.history.start();
	App.Views.AddTask = Backbone.View.extend({
		el:'#addTask',

		events:{
			'submit':'submit'
		},

		initialize:function () {
			
		},

		submit:function (event) {
			event = event||window.event;
			event.preventDefault();

			var newTaskTitle = $(event.currentTarget).find('input[type=text]').val();

			var newTask = new App.Models.Task({title:newTaskTitle});
			this.collection.add(newTask);
		}

	});

	$('.tasks').html(tasksView.render().el);

	var addTaskView = new App.Views.AddTask({collection:tasks});
	new App.Views.Tasks({collection: tasks});
	App.Models.Person = Backbone.Model.extend({
		defaults:{
			name: 'Ievgen',
			age: 25,
			job: 'Front-end developer'
		},

		validate: function ( attrs ) {
			
			if(attrs.age <= 0){
				return 'Age can be null or minus';
			}
			if(!attrs.name){
				return 'Input the name';
			}
		},
		walk: function () {
			return this.get('name') + ' is walking';
		}
	});

	App.Collections.Persons = Backbone.Collection.extend({
		model: App.Models.Person,
	});

	App.Views.Persons = Backbone.View.extend({
		tagName:'ul',

		initialize: function () {
			
		},

		render: function () {
			this.collection.each(function(person) {
				var personView = new App.Views.Person({model:person});
				this.$el.append(personView.el);
			},this);	
			return this;
		}

	});


	App.Views.Person = Backbone.View.extend({
		tagName:'li',

		template: template('person-id'),

		initialize:function () {
			this.render();
		},
		
		render:function() {
			this.$el.html( this.template( this.model.toJSON() ) );
			return this;
		}
	});

	var person = new App.Models.Person;
	var personView = new App.Views.Person({model:person});

	var personsCollection = new App.Collections.Persons([
		{
			name:'Andrii',
			age:23,
			job:'Worker'
		},
		{
			name:'Ievgen',
			age:25,
			job:'Developer'
		},
		{
			name:'Sergii',
			age:29,
			job:'Taxy driver'
		},
	]);

	var personsView = new App.Views.Persons({collection: personsCollection});
	$(document.body).append(personsView.render().el);

}());
