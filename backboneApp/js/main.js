$(function () {
	window.App = {
		Models:{},
		Views:{},
		Collections:{},
	};

	window.template = function (id) {
		return _.template($('#'+id).html());
	};

	App.Models.Task = Backbone.Model.extend({
		validate: function (attrs) {
			if(!$.trim(attrs.title)){
				createLog('Error');
				return true;
			}
			else {
				createLog('Well done');
				return false;
			}
		}
	});

	App.Views.Task = Backbone.View.extend({
		initialize:function () {
			this.model.on('change', this.render, this);	
			this.model.on('destroy', this.remove, this);
			
		},
		tagName: 'li',
		template: template('taskTemplate'),

		render: function () {
			var template = this.template(this.model.toJSON());
			this.$el.html(template);
			return this;
		},
		events:{
			'click .edit'		:'editTask',
			'click .delete'		:'destroy',
			'mouseover .delete'	:'warning',
			'mouseout .delete'	:'removeWarning',
		},
		editTask:function () {
			var taskTitle = this.model.get('title')
			var newTaskTitle = prompt('How did you want rename the task?',taskTitle);

			if ((newTaskTitle!=taskTitle)&&(newTaskTitle!==null)){
				this.model.set('title',newTaskTitle,{validate:true});
			}
			else{
				createLog('Information');
			}
			
			
		},
		destroy:function () {
			this.model.destroy();
		},
		remove:function() {
			this.$el.remove();
			this.removeWarning();
		},
		warning:function () {
			createLog('Warning');
		},
		removeWarning:function () {
			var element = $('#logger h4');
			if(element.html()=='Warning'){
				$('#logger').fadeOut(3000, function() {
					$('#logger').removeClass().html('');	
				});
			}
		}
	});

	App.Collections.Task = Backbone.Collection.extend({
		model: App.Models.Task
	});

	App.Views.Tasks = Backbone.View.extend({
		tagName: 'ul',
		
		initialize:function () {
			this.collection.on('add',this.addOne,this);
		},
		render: function () {
			
			this.collection.each(this.addOne , this);

			return this;
		},
		addOne: function(task){
			var taskView = new App.Views.Task({model: task});
			this.$el.append(taskView.render().el)
			
		}
	});
	var tasks = new App.Collections.Task([
		{ 
			title:'Go to work',
			priority: 5
		},
		{
			title: 'Get letters',
			priority: 3
		},
		{	
			title: 'Go to the shop',
			priority: 4
		}
			]);
	var tasksView = new App.Views.Tasks({collection: tasks});

	var createLog = function(title){
		var logModel = loggs.findWhere({title:title});
		var logView = new App.Views.Logg({model:logModel});
		logView.render();
	}

	App.Views.AddTask = Backbone.View.extend({
		el:'#addTask',

		events:{
			'submit':'submit'
		},

		initialize:function () {
			
		},

		submit:function (event) {
			event = event||window.event;
			event.preventDefault();
			var newTaskTitle = $(event.currentTarget).find('input[type=text]').val();
			if(!!$.trim(newTaskTitle)){
				var newTask = new App.Models.Task({title:newTaskTitle});
				this.collection.add(newTask);
				$(event.currentTarget).find('input[type=text]').val('');
				createLog('Well done');
			}
			else{
				createLog('Error');
			}
		}

	});

	App.Models.Logg = Backbone.Model.extend({
		defaults:{
			title:'Warning',
			message:'You did some trouble',
			cls:'alert'
		}
	});

	App.Views.Logg = Backbone.View.extend({
		el:'#logger',
		template : template('loggerTemplate'),
		initialize:function () {

		},
		render:function () {
			var template = this.template(this.model.toJSON());
			var clsName = this.model.get('cls');
			var thisTitle = this.model.get('title');
			this.$el.html(template).addClass(clsName).fadeIn(1500);
				if (thisTitle!='Warning') {
					this.$el.fadeOut(3000,function () {
						$('#logger').removeClass().html('');
				
					});
			};
		}
		
	});

	App.Views.Loggs = Backbone.View.extend({});

	App.Collections.Loggs = Backbone.Collection.extend({
		model :App.Models.Logg
	});

	var loggs = new App.Collections.Loggs([
		{
			title:'Warning',
			message:'You will do some changes in the model',
			cls : 'alert alert-block'	
		},
		{
			title:'Error',
			message:'You can\'t do that',
			cls : 'alert alert-error'	
		},
			{
			title:'Well done',
			message:'You successfully do your action',
			cls : 'alert alert-success'	
		},
		{
			title:'Information',
			message:'If you do this that will do some change',
			cls : 'alert alert-info'	
		},
	]);
	$('.tasks').html(tasksView.render().el);

	var addTaskView = new App.Views.AddTask({collection:tasks});
})